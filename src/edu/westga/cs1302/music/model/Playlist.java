package edu.westga.cs1302.music.model;

import java.util.ArrayList;

import edu.westga.cs1302.music.resources.UI;

/**
 * This class represents a playlist.
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class Playlist {

	public static final int SECONDS_PER_MINUTE = 60;
	public static final int SECONDS_ROUNDING_THRESHOLD = 30;
	public static final String DEFAULT_NAME = "Untitled";
	private String name;
	private ArrayList<Song> songs;

	/**
	 * Constructs a default playlist.
	 * 
	 * @precondition none
	 * @postcondition getName() == DEFAULT_NAME && size() == 0
	 */
	public Playlist() {
		this.name = DEFAULT_NAME;
		this.songs = new ArrayList<Song>();
	}

	/**
	 * Constructs a playlist with the specified name and no songs.
	 * 
	 * @precondition name != null && !name.isEmpty()
	 * @postcondition getName() == name && size() == 0
	 * @param name the specified name of this playlist
	 */
	public Playlist(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_EMPTY);
		}
		this.name = name;
		this.songs = new ArrayList<Song>();
	}

	/**
	 * Returns the name of this playlist.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name of this playlist.
	 * 
	 * @precondition name != null && !name.isEmpty().
	 * @postcondition getName() == name
	 * @param name the name to set
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.PLAYLIST_NAME_EMPTY);
		}
		this.name = name;
	}

	/**
	 * Returns the songs of this playlist.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the songs
	 */
	public ArrayList<Song> getSongs() {
		return this.songs;
	}

	/**
	 * Adds the song to the playlist (allows duplicates)
	 * 
	 * @precondition song != null
	 * @postcondition size() == size()@prev + 1
	 * @param song the song to be added
	 * @return true if add successful
	 */
	public boolean add(Song song) {
		if (song == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SONG_NULL);
		}
		return this.songs.add(song);
	}

	/**
	 * Returns the size of the collection of songs (the number of songs).
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the size of the collection
	 */
	public int size() {
		return this.songs.size();
	}

	/**
	 * Returns the duration of the playlist in minutes, rounded to the nearest
	 * minute. If the number of seconds is strictly less than 30, round to the
	 * previous minute. If the number of seconds is 30 or more, round to the next
	 * minute. For example, if the duration is 5 minutes and 29 seconds, return 5.
	 * If the duration is 10 minutes and 30 seconds, return 11 minutes.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the rounded duration in minutes
	 */
	public int getDuration() {
		int minutes = 0;
		int seconds = 0;

		for (Song song : this.songs) {
			minutes += song.getMinutes();
			seconds += song.getSeconds();
		}

		minutes += seconds / SECONDS_PER_MINUTE;
		if (seconds % SECONDS_PER_MINUTE >= SECONDS_ROUNDING_THRESHOLD) {
			minutes++;
		}
		return minutes;
	}

	@Override
	public String toString() {
		return this.name + ": " + this.size() + " songs.";
	}

	/**
	 * Returns shortest song from the playlist. If multiple songs have the same
	 * shortest length, it will return the first encountered song. If list is empty
	 * return null.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return shortest song (first in order) or null if no songs
	 */
	public Song getShortestSong() {

		if (this.songs.size() == 0) {
			return null;
		}

		Song shortestSong = this.songs.get(0);
		for (Song currentSong : this.songs) {
			if (currentSong.getMinutes() == shortestSong.getMinutes()) {
				if (currentSong.getSeconds() < shortestSong.getSeconds()) {
					shortestSong = currentSong;
				}
			}
			if (currentSong.getMinutes() < shortestSong.getMinutes()) {
				shortestSong = currentSong;
			}
		}
		return shortestSong;
	}

	/**
	 * Returns longest song from the playlist. If multiple songs have the same
	 * longest length, it will return the first encountered song.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return longest song (first in order) or null if no songs
	 */
	public Song getLongestSong() {

		if (this.songs.size() == 0) {
			return null;
		}

		Song longestSong = this.songs.get(0);
		for (Song currentSong : this.songs) {
			if (currentSong.getMinutes() == longestSong.getMinutes()) {
				if (currentSong.getSeconds() > longestSong.getSeconds()) {
					longestSong = currentSong;
				}
			}
			if (currentSong.getMinutes() > longestSong.getMinutes()) {
				longestSong = currentSong;
			}
		}
		return longestSong;
	}

	/**
	 * Deletes the specified song from the playlist.
	 * 
	 * @precondition none
	 * @postcondition if found, size() == size()@prev - 1
	 * @param song the song to delete
	 * @return true if the song was found and deleted from the playlist, false
	 *         otherwise
	 */
	public boolean remove(Song song) {
		return this.songs.remove(song);
	}

	/**
	 * Adds all the songs to the playlist.
	 * 
	 * @precondition songs != null
	 * @postcondition size() == size()@prev + songs.size()
	 * @param songs the songs to add to the playlist
	 * @return true if successful
	 */
	public boolean addAll(ArrayList<Song> songs) {
		if (songs == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SONGS_NULL);
		}
		return this.songs.addAll(songs);
	}
}
