package edu.westga.cs1302.music.model;

import edu.westga.cs1302.music.resources.UI;

/**
 * The class Validator.
 * 
 * @author CS 1302
 * @version Fall 2021
 */
public class Validator {

	private String minutesError;
	private String secondsError;
	private String yearError;

	/**
	 * Instantiates a new Validator.
	 * 
	 * @precondition none
	 * @postcondition getMinutesError().isEmpty() && getSecondsError().isEmpty() &&
	 *                getYearError().isEmpty()
	 */
	public Validator() {
		this.reset();
	}

	/**
	 * Returns the minutesError of this Validator.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the minutesError
	 */
	public String getMinutesError() {
		return this.minutesError;
	}

	/**
	 * Returns the secondsError of this Validator.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the secondsError
	 */
	public String getSecondsError() {
		return this.secondsError;
	}

	/**
	 * Returns the yearError of this Validator.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the yearError
	 */
	public String getYearError() {
		return this.yearError;
	}

	/**
	 * Found error.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if a preceding call to a validation method detected an error
	 */
	public boolean foundError() {
		return !this.minutesError.isEmpty() || !this.secondsError.isEmpty() || !this.yearError.isEmpty();
	}

	/**
	 * Validate minutes. Removes trailing and leading spaces from specified
	 * minutesString. Checks if the resulting string represents a valid minutes
	 * value and sets a suitable error message. A valid value has to be a whole
	 * number >= 0 and can only have at most three digits.
	 *
	 * @precondition none
	 * @postcondition getMinutesError().isEmpty() if the passed-in string represents
	 *                a valid value for the minutes variable; otherwise
	 *                getMinutesError() returns a suitable error message
	 *
	 * @param minutesString the string representing minutes
	 * @return the value represented by minutesString after leading and trailing
	 *         spaces have been removed; null if minutesString does not represent a
	 *         valid minutes value
	 */
	public Integer validateMinutes(String minutesString) {
//		this.minutesError = "invalid minutes";	
//		int minuteNumber = 0;
//		minutesString = minutesString.trim();		
//		try {
//			if (minutesString == null || minutesString.isBlank()) {
//				this.minutesError = "This field is required";
//			}		
//			System.out.println(minutesString.matches("\\d{3}"));
//			if (!minutesString.matches("\\d{3}")) {
//				
//				this.minutesError = "Whole number required, with at most 3 digits.";
//			}
//			minuteNumber = Integer.parseInt(minutesString);
//			if (minuteNumber > 999) {
//				this.minutesError = "Whole number required, with at most 3 digits.";
//			}	
//		} catch (NumberFormatException exception) {
//			System.err.println("Must enter a number");
//		} catch (IllegalArgumentException exception) {
//			System.err.println(exception.getMessage());
//		} catch (NullPointerException exception) {
//			System.err.println(exception.getMessage());
//		}
		try {
			String minuteString = minutesString.trim();
		
			//if (minuteString.length() > 3) {
			if (minuteString.matches("\\d{4}")) {
				this.minutesError = "Enter a Integer thats 3 or less";
				return null;
			}
			try {
				int minutes = Integer.parseInt(minuteString);
				if (minutes < 0) {
					this.minutesError = "Enter a Integer 0 or Greater";
					return null;
				}
				return minutes;
			}catch (NumberFormatException exception) {
				this.minutesError = "Enter a Number";
			}
		} catch (NullPointerException exception) {
			this.minutesError = "Enter a Number";
			//exception.getMessage();
		} 
	
		return null;
	}

	/**
	 * Validate seconds. Removes trailing and leading spaces from specified
	 * secondsString. Checks if the resulting string represents a valid seconds
	 * value and sets a suitable error message. A valid value has to be a number >=
	 * 0 and can only have 2 digits.
	 *
	 * @precondition none
	 * @postcondition getSecondsError().isEmpty() if the passed in string represents
	 *                a valid value for the seconds variable; otherwise
	 *                getSecondsError() returns a suitable error message
	 *
	 * @param secondsString the string representing seconds
	 * @return the value represented by secondsString after leading and trailing
	 *         spaces have been removed; null if secondsString does not represent a
	 *         valid seconds value
	 */
	public Integer validateSeconds(String secondsString) {
		//this.secondsError = "invalid seconds";
//		int seconds = 0;
//		try {	
//			seconds = Integer.parseInt(secondsString.trim());
//		} catch (IllegalArgumentException exception) {
//			System.err.println(exception.getMessage());
//		}
		//return seconds;
		try {
			String userInput = secondsString.trim();
			if (userInput.length() > 2) {
				this.secondsError = "Enter a Intenger thats 2 or less";
				return null;
			}
			
			int seconds = Integer.parseInt(userInput);
			if (seconds < 0 || seconds > 59) {
				this.secondsError = "Enter a Integer between 0 & 59";
				return null;
			}
			return seconds;
		} catch (NullPointerException exception) {
			this.secondsError = "Enter a Number";
		} catch (NumberFormatException exception) {
			this.secondsError = "Enter a Number";
		}
		return null;
	}

	/**
	 * Validate the specified yearString. Removes trailing and leading spaces from
	 * specified yearString. Checks if the resulting string represents a valid year
	 * and sets a suitable error message. A valid year has to be a number with four
	 * digits, where the first number cannot be 0.
	 *
	 * @precondition none
	 * @postcondition getYearError().isEmpty() if the passed in string represents a
	 *                valid year; otherwise getYearError() returns an suitable error
	 *                message
	 * 
	 * @param yearString the string representing the year of a song
	 * @return the value represented by yearString after leading and trailing spaces
	 *         have been removed; null if yearString does not represent a valid year
	 */
	public Integer validateYear(String yearString) {
		//this.yearError = "invalid year";
//		int year = 0;
//		try {
//			year = Integer.parseInt(yearString.trim());
//		} catch (IllegalArgumentException exception) {
//			System.err.println(exception.getMessage());
//		}
//		return year;
		try {
			String input = yearString.trim();
			if (!input.matches("\\d{4}")) {
				//if (input.length() != 4) {
				this.yearError = "Whole number required, with 4 digits.";
				return null;
			}
			
			if (input.matches("^0(\\d+)")) {
				//input.charAt(0) == '0'
				this.yearError = "0 cannot be at the front of the year";
				return null;
			}
			
			int year = Integer.parseInt(input);
			if (year < 1900) {
				this.yearError = "Enter a year 1900 or greater";
				return null;
			}
			return year;
			
		} catch (NullPointerException exception) {
			this.yearError = "Enter a year";
		} catch (NumberFormatException exception) {
			this.yearError = UI.ExceptionMessages.YEAR_INVALID;
		}
		
		return null;
	}

	/**
	 * Resets validator.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void reset() {
		this.minutesError = "";
		this.secondsError = "";
		this.yearError = "";
	}
}
