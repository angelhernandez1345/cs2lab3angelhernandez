package edu.westga.cs1302.music.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import edu.westga.cs1302.music.datatier.MusicFileReader;
import edu.westga.cs1302.music.datatier.MusicFileWriter;
import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.model.Validator;
import edu.westga.cs1302.music.output.ReportGenerator;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * The class MusicViewModel.
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class MusicViewModel {

	private Playlist playlist;
	private ReportGenerator report;

	private StringProperty titleProperty;
	private StringProperty artistProperty;
	private StringProperty minutesProperty;
	private StringProperty secondsProperty;
	private StringProperty yearProperty;
	private ObjectProperty<Song> selectedSongProperty;

	private StringProperty errorProperty;
	private StringProperty minutesErrorProperty;
	private StringProperty secondsErrorProperty;
	private StringProperty yearErrorProperty;

	private ListProperty<Song> songsProperty;
	private StringProperty playlistSummaryProperty;

	/**
	 * Instantiates a music view model.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MusicViewModel() {

		this.playlist = new Playlist("Cruising");
		this.report = new ReportGenerator();

		this.titleProperty = new SimpleStringProperty();
		this.artistProperty = new SimpleStringProperty();
		this.minutesProperty = new SimpleStringProperty();
		this.secondsProperty = new SimpleStringProperty();
		this.yearProperty = new SimpleStringProperty();
		this.selectedSongProperty = new SimpleObjectProperty<Song>();

		this.minutesErrorProperty = new SimpleStringProperty();
		this.secondsErrorProperty = new SimpleStringProperty();
		this.yearErrorProperty = new SimpleStringProperty();
		this.errorProperty = new SimpleStringProperty();

		this.songsProperty = new SimpleListProperty<Song>(FXCollections.observableArrayList(this.playlist.getSongs()));
		this.playlistSummaryProperty = new SimpleStringProperty();
	}

	/**
	 * The title property.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the title property
	 */
	public StringProperty titleProperty() {
		return this.titleProperty;
	}

	/**
	 * The artist property.
	 * 
	 * @precondition
	 * @postcondition
	 * @return the artist property
	 */
	public StringProperty artistProperty() {
		return this.artistProperty;
	}

	/**
	 * The year property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year property
	 */
	public StringProperty yearProperty() {
		return this.yearProperty;
	}

	/**
	 * The minutes property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the minutes property
	 */
	public StringProperty minutesProperty() {
		return this.minutesProperty;
	}

	/**
	 * The seconds property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the seconds property
	 */
	public StringProperty secondsProperty() {
		return this.secondsProperty;
	}

	/**
	 * The selected song property.
	 *
	 * @precondition none
	 * @postcondition none
	 * @return the selected song property
	 */
	public ObjectProperty<Song> selectedSongProperty() {
		return this.selectedSongProperty;
	}

	/**
	 * Returns the errorProperty of this MusicViewModel.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the errorProperty
	 */
	public StringProperty errorProperty() {
		return this.errorProperty;
	}

	/**
	 * Returns the minutesErrorProperty of this MusicViewModel.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the minutesErrorProperty
	 */
	public StringProperty minutesErrorProperty() {
		return this.minutesErrorProperty;
	}

	/**
	 * Returns the secondsErrorProperty of this MusicViewModel.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the secondsErrorProperty
	 */
	public StringProperty secondsErrorProperty() {
		return this.secondsErrorProperty;
	}

	/**
	 * Returns the yearErrorProperty of this MusicViewModel.
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the yearErrorProperty
	 */
	public StringProperty yearErrorProperty() {
		return this.yearErrorProperty;
	}

	/**
	 * The songs property.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the list of songs property
	 */
	public ListProperty<Song> songsProperty() {
		return this.songsProperty;
	}

	/**
	 * Playlist summary property.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the playlist summary property
	 */
	public StringProperty playlistSummaryProperty() {
		return this.playlistSummaryProperty;
	}

	/**
	 * Update selected song
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if successful
	 */
	public boolean updateSong() {
		Song song = this.selectedSongProperty.get();
		if (song == null) {
			return false;
		}

		Validator validator = new Validator();

		String title = this.titleProperty.get();
		String artist = this.artistProperty.get();
		Integer minutes = validator.validateMinutes(this.minutesProperty.get());
		Integer seconds = validator.validateSeconds(this.secondsProperty.get());
		Integer year = validator.validateYear(this.yearProperty.get());

		this.setErrorMessage(validator);
		if (validator.foundError()) {
			return false;
		}

		try {
			song.setTitle(title);
			song.setArtist(artist);
			song.setMinutes(Integer.valueOf(minutes));
			song.setSeconds(Integer.valueOf(seconds));
			song.setYear(Integer.valueOf(year));
			this.resetSongEntryProperties();
			return true;

		} catch (IllegalArgumentException ex) {
			this.errorProperty.set("");
			this.errorProperty.set(ex.getMessage());
		}
		return false;
	}

	private void resetSongEntryProperties() {
		this.clearSongProperties();
		this.updateSummary();
		this.songsProperty.set(FXCollections.observableArrayList(this.playlist.getSongs()));

	}

	private void updateSummary() {
		String summaryReport = this.report.buildFullSummaryReport(this.playlist);
		this.playlistSummaryProperty.set(summaryReport);
	}

	/**
	 * Resets all properties.
	 */
	public void clearSongProperties() {
		this.titleProperty.set("");
		this.artistProperty.set("");
		this.minutesProperty.set("");
		this.secondsProperty.set("");
		this.yearProperty.set("");

		this.minutesErrorProperty.set("");
		this.secondsErrorProperty.set("");
		this.yearErrorProperty.set("");
		this.errorProperty.set("");
	}

	/**
	 * Adds a song to the playlist.
	 *
	 * @return true if song added successfully, false otherwise
	 */
	public boolean addSong() {

		Validator validator = new Validator();

		String title = this.titleProperty.get();
		String artist = this.artistProperty.get();

		Integer minutes = validator.validateMinutes(this.minutesProperty.get());
		Integer seconds = validator.validateSeconds(this.secondsProperty.get());
		Integer year = validator.validateYear(this.yearProperty.get());

		this.setErrorMessage(validator);
		if (validator.foundError()) {
			return false;
		}
		try {
			Song song = new Song(title, artist, minutes, seconds, year);
			if (this.playlist.add(song)) {
				this.resetSongEntryProperties();
				return true;
			}
		} catch (IllegalArgumentException ex) {
			this.errorProperty.set("");
			this.errorProperty.set(ex.getMessage());
		}
		return false;

	}

	private void setErrorMessage(Validator validator) {
		this.minutesErrorProperty.set(validator.getMinutesError());
		this.secondsErrorProperty.set(validator.getSecondsError());
		this.yearErrorProperty.set(validator.getYearError());
	}

	/**
	 * Delete selected song from the playlist.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if song successfully deleted, false otherwise
	 */
	public boolean deleteSong() {
		Song song = this.selectedSongProperty.get();
		if (this.playlist.remove(song)) {
			this.resetSongEntryProperties();
			return true;
		}
		return false;
	}

	/**
	 * Save the playlist to the specified File.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @param file the file to save the playlist to
	 */
	public void savePlaylist(File file) {
		MusicFileWriter writer = new MusicFileWriter(file);
		try {
			writer.write(this.playlist.getSongs());
		} catch (FileNotFoundException ex) {
			this.errorProperty.set(ex.getMessage());
		}
	}

	/**
	 * Loads the playlist file using the specified File object.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param file The file to load the playlist from
	 */
	public void loadPlaylist(File file) {
		MusicFileReader reader = new MusicFileReader(file);
		try {
			ArrayList<Song> songs = reader.loadAllSongs();
			if (this.playlist.addAll(songs)) {
				this.resetSongEntryProperties();
			}
		} catch (FileNotFoundException ex) {
			this.errorProperty.set(ex.getMessage());
		}
	}
}
