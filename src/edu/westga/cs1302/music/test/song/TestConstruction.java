package edu.westga.cs1302.music.test.song;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.resources.UI;

public class TestConstruction {

	@Test
	void testValidConstruction() {
		Song song = new Song("Save Your Tears", "The Weeknd", 4, 8, 2021);
		assertAll(() -> assertEquals("Save Your Tears", song.getTitle()),
				() -> assertEquals("The Weeknd", song.getArtist()), () -> assertEquals(4, song.getMinutes()),
				() -> assertEquals(8, song.getSeconds()), () -> assertEquals(2021, song.getYear()));
	}

	@Test
	void testNullTitle() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song(null, "The Weeknd", 4, 8, 2021));
		assertEquals(UI.ExceptionMessages.TITLE_NULL, exception.getMessage());
	}

	@Test
	void testEmptyTitle() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("", "The Weeknd", 4, 8, 2021));
		assertEquals(UI.ExceptionMessages.TITLE_EMPTY, exception.getMessage());
	}

	@Test
	void testNullArtist() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("Save Your Tears", null, 4, 8, 2021));
		assertEquals(UI.ExceptionMessages.ARTIST_NULL, exception.getMessage());
	}

	@Test
	void testEmptyArtist() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("Save Your Tears", "", 4, 8, 2021));
		assertEquals(UI.ExceptionMessages.ARTIST_EMPTY, exception.getMessage());
	}

	@Test
	void testNegativeMinutes() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("Save Your Tears", "The Weeknd", -1, 0, 2021));
		assertEquals(UI.ExceptionMessages.MINUTES_NEGATIVE, exception.getMessage());
	}

	@Test
	void testNegativeSeconds() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("Save Your Tears", "The Weeknd", 0, -3, 2021));
		assertEquals(UI.ExceptionMessages.SECONDS_INVALID, exception.getMessage());
	}

	@Test
	void testDurationZero() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("Save Your Tears", "The Weeknd", 0, 0, 2021));
		assertEquals(UI.ExceptionMessages.DURATION_ZER0, exception.getMessage());
	}

	@Test
	void testNegativeYear() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("Save Your Tears", "The Weeknd", 4, 8, -2021));
		assertEquals(UI.ExceptionMessages.YEAR_INVALID, exception.getMessage());
	}

	@Test
	void testBelowMinimunYear() {
		IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
				() -> new Song("Save Your Tears", "The Weeknd", 4, 8, 1899));
		assertEquals(UI.ExceptionMessages.YEAR_INVALID, exception.getMessage());
	}
	
	@Test
	void testMinimunYear() {
		Song song = new Song("My Tiger Lily", "Arthur Collins", 2, 13, 1900);
		assertAll(() -> assertEquals("My Tiger Lily", song.getTitle()),
				() -> assertEquals("Arthur Collins", song.getArtist()), () -> assertEquals(2, song.getMinutes()),
				() -> assertEquals(13, song.getSeconds()), () -> assertEquals(1900, song.getYear()));
	}

}
