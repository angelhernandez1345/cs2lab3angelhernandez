package edu.westga.cs1302.music.test.playlist;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;

/**
 * Ensures correct functionality of getShortestSong() method.
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class TestGetShortestSong {

	@Test
	void testEmptyPlaylist() {
		Playlist mySongs = new Playlist("My playlist");
		assertNull(mySongs.getShortestSong());
	}

	@Test
	void testFirstSongIsShortest() {
		Song song1 = new Song("Title1", "Artist1", 1, 31, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 32, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 33, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(song1, mySongs.getShortestSong());
	}

	@Test
	void testMultipleShortest() {
		Song song1 = new Song("Title1", "Artist1", 1, 31, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 32, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 31, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(song1, mySongs.getShortestSong());
	}

	@Test
	void testShortestInMiddle() {
		Song song1 = new Song("Title1", "Artist1", 1, 31, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 33, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(song2, mySongs.getShortestSong());
	}

	@Test
	void testShortestLast() {
		Song song1 = new Song("Title1", "Artist1", 1, 31, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 5, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(song3, mySongs.getShortestSong());
	}

	@Test
	void testMultipleShortestMiddle() {
		Song song1 = new Song("Title1", "Artist1", 1, 31, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 30, 2021);
		Song song4 = new Song("Title4", "Artist4", 1, 45, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		mySongs.add(song4);
		assertEquals(song2, mySongs.getShortestSong());
	}

}
