package edu.westga.cs1302.music.test.playlist;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.music.model.Playlist;
import edu.westga.cs1302.music.model.Song;

/**
 * Ensures correct functionality of the getDuration() method.
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class TestGetDuration {

	@Test
	void testExactMinutes() {
		Song song1 = new Song("Title1", "Artist1", 1, 0, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 0, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 0, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(3, mySongs.getDuration());
	}

	@Test
	void testOneMinuteAdded() {
		Song song1 = new Song("Title1", "Artist1", 1, 20, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 20, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 20, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(4, mySongs.getDuration());
	}

	@Test
	void testRoundUpAtThreshold() {
		Song song1 = new Song("Title1", "Artist1", 1, 30, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 30, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(5, mySongs.getDuration());
	}

	@Test
	void testRoundDownWellBelowThreshold() {
		Song song1 = new Song("Title1", "Artist1", 1, 4, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 30, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(4, mySongs.getDuration());
	}

	@Test
	void testRoundDownRightBelowThreshold() {
		Song song1 = new Song("Title1", "Artist1", 1, 29, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 30, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(4, mySongs.getDuration());
	}

	@Test
	void testRoundUpRightAboveThreshold() {
		Song song1 = new Song("Title1", "Artist1", 1, 31, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 30, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(5, mySongs.getDuration());
	}

	@Test
	void testRoundUpWellAboveThreshold() {
		Song song1 = new Song("Title1", "Artist1", 1, 45, 2021);
		Song song2 = new Song("Title2", "Artist2", 1, 30, 2021);
		Song song3 = new Song("Title3", "Artist3", 1, 30, 2021);
		Playlist mySongs = new Playlist("My playlist");
		mySongs.add(song1);
		mySongs.add(song2);
		mySongs.add(song3);
		assertEquals(5, mySongs.getDuration());
	}

}
