package edu.westga.cs1302.music.resources;

/**
 * Defines strings that will be displayed in the UI (user interface).
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class UI {

	/**
	 * Defines string messages for exceptions for this music application.
	 */
	public static final class ExceptionMessages {

		public static final String TITLE_NULL = "Title cannot be null.";
		public static final String TITLE_EMPTY = "Title cannot be empty.";
		public static final String ARTIST_NULL = "Artist cannot be null.";
		public static final String ARTIST_EMPTY = "Artist cannot be empty.";
		public static final String DURATION_ZER0 = "Song must be at least 1 second in length.";
		public static final String MINUTES_NEGATIVE = "Number of minutes cannot be negative.";
		public static final String SECONDS_INVALID = "Number of seconds cannot be negative or greater than 59.";
		public static final String YEAR_INVALID = "Year cannot be negative or below 1900.";

		public static final String PLAYLIST_NAME_NULL = "Name cannot be null.";
		public static final String PLAYLIST_NAME_EMPTY = "Name cannot be empty.";
		public static final String SONG_NULL = "Song cannot be null.";
		public static final String PLAYLIST_FILE_NULL = "MPL file cannot be null.";
		public static final String SONGS_NULL = "Songs collection cannot be null.";

	}

	/**
	 * Defines string messages to be used in UI messages.
	 */
	public static final class Text {

		public static final String COMMA_SPACE_SEPARATOR = ", ";

	}
}
