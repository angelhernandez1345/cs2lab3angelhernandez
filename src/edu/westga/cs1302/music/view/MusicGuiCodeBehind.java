package edu.westga.cs1302.music.view;

import java.io.File;

import edu.westga.cs1302.music.model.Song;
import edu.westga.cs1302.music.viewmodel.MusicViewModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

/**
 * The controller behind the GUI file MusicGui.fxml
 * 
 * @author CS1302
 * @version Fall 2021
 */
public class MusicGuiCodeBehind {

	private MusicViewModel theViewModel;
	private ObjectProperty<Song> selectedSong;

	@FXML
	private Pane pane;

	@FXML
	private MenuItem fileOpenMenuItem;

	@FXML
	private MenuItem fileSaveMenuItem;

	@FXML
	private TextField titleTextField;

	@FXML
	private TextField artistTextField;

	@FXML
	private TextField minutesTextField;

	@FXML
	private TextField secondsTextField;

	@FXML
	private TextField yearTextField;

	@FXML
	private Label yearInvalidLabel;

	@FXML
	private Label minutesInvalidLabel;

	@FXML
	private Label errorLabel;

	@FXML
	private Label secondsInvalidLabel;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private Button clearButton;

	@FXML
	private ListView<Song> songsListView;

	@FXML
	private Button deleteButton;

	@FXML
	private TextArea summaryTextArea;

	/**
	 * Instantiates a new gui code behind.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MusicGuiCodeBehind() {
		this.theViewModel = new MusicViewModel();
		this.selectedSong = new SimpleObjectProperty<Song>();
	}

	/**
	 * Initializes the GUI components, binding them to the view properties
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.setupListenerForListView();
	}

	private void setupListenerForListView() {
		this.songsListView.getSelectionModel().selectedItemProperty().addListener((observable, oldAuto, newSong) -> {
			if (newSong != null) {
				this.titleTextField.textProperty().set(newSong.getTitle());
				this.artistTextField.textProperty().set(newSong.getArtist());
				Integer minutes = newSong.getMinutes();
				this.minutesTextField.textProperty().set(minutes.toString());
				Integer seconds = newSong.getSeconds();
				this.secondsTextField.textProperty().set(seconds.toString());
				Integer year = newSong.getYear();
				this.yearTextField.textProperty().set(year.toString());
				this.selectedSong.set(newSong);
			}
		});
	}

	private void bindComponentsToViewModel() {
		this.titleTextField.textProperty().bindBidirectional(this.theViewModel.titleProperty());
		this.artistTextField.textProperty().bindBidirectional(this.theViewModel.artistProperty());
		this.yearTextField.textProperty().bindBidirectional(this.theViewModel.yearProperty());
		this.minutesTextField.textProperty().bindBidirectional(this.theViewModel.minutesProperty());
		this.secondsTextField.textProperty().bindBidirectional(this.theViewModel.secondsProperty());
		this.selectedSong.bindBidirectional(this.theViewModel.selectedSongProperty());

		this.minutesInvalidLabel.textProperty().bindBidirectional(this.theViewModel.minutesErrorProperty());
		this.secondsInvalidLabel.textProperty().bindBidirectional(this.theViewModel.secondsErrorProperty());
		this.yearInvalidLabel.textProperty().bindBidirectional(this.theViewModel.yearErrorProperty());
		this.errorLabel.textProperty().bindBidirectional(this.theViewModel.errorProperty());

		this.songsListView.itemsProperty().bind(this.theViewModel.songsProperty());
		this.summaryTextArea.textProperty().bind(this.theViewModel.playlistSummaryProperty());
	}

	@FXML
	private void onAddSong() {
		this.theViewModel.addSong();
	}

	@FXML
	private void onUpdateSong() {
		this.theViewModel.updateSong();
	}

	@FXML
	private void onDeleteSong() {
		this.theViewModel.deleteSong();
	}

	@FXML
	private void onClearSong() {
		this.theViewModel.clearSongProperties();
	}

	@FXML
	private void onFileOpen() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);
		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(owner);
		if (selectedFile != null) {
			this.theViewModel.loadPlaylist(selectedFile);
		}
	}

	@FXML
	private void onFileSave() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);
		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(owner);
		if (selectedFile != null) {
			this.theViewModel.savePlaylist(selectedFile);
		}
	}

	private void setFileFilters(FileChooser fileChooser) {
		ExtensionFilter filter = new ExtensionFilter("Music Playlist", "*.mpl");
		fileChooser.setInitialDirectory(new File(System.getProperty("user.home") + "/Desktop"));
		fileChooser.getExtensionFilters().add(filter);
		filter = new ExtensionFilter("All Files", "*.*");
		fileChooser.getExtensionFilters().add(filter);
	}

}
